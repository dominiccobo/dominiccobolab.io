---
title: "Final Year Product"
date: 2019-12-26T01:47:57Z
featured: true
description: "On Addressing the Cognitive Issues Introduced by Proliferation of Tools for Developers"
tags: ["university", "academic", "research"]
image: ""
link: "https://dominiccobo-fyp.github.io/docs/"
weight: 500
sitemap:
  priority : 0.8
---

The information and tools interacted with by developers for daily problem solving has exponentially proliferated in recent years. 

This increase is not without consequence. Navigating, filtering and using the required information is cognitively tasking and time consuming. The quest for more informed choices, in knowledge of the mere existence of useful information, can often distract from the original task at hand. 

Mechanisms for enrichment of developer's context exist, but require separate implementation efforts across different environments, introducing apparent usage barriers.

In this project we seek to create a platform that facilitates the integration of a variety of sources with an easy, replicable implementation across environments, producing tailored queries and yielding responses for a given problem context. 
